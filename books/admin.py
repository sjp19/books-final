from django.contrib import admin
from books.models import Book, BookReview, Magazine, Genre, Issue

admin.site.register(Book)
admin.site.register(BookReview)
admin.site.register(Magazine)
admin.site.register(Genre)
admin.site.register(Issue)
