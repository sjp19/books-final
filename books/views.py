from django.shortcuts import get_object_or_404, render, redirect, get_list_or_404, HttpResponseRedirect
from django.contrib.auth.decorators import login_required


from books.models import Book, Magazine, Genre# To access database abstraction model and Recipe table
from books.forms import MagazineForm, BookForm

# Create your views here


# login required decorator
# @login_required
def list_reviews(request):
    # how do I get user who made the request?
    book_reviews_by_this_user = request.user.reviews_by_this_user.all()
    # How do I get all model instances made by that user?
    context = {
        "reviews_queryset": book_reviews_by_this_user
    }
    return render(request, 'book/review_list.html', context)













def show_books(request):
    # In the render function, the third parameter will be the data from the database to be sent to the template "books/book_list.html"
    # {'books':books}

    books = Book.objects.all()
    # context = {
    #     "books": books if Book else [{"name": "Repository Empty"}]
    # }

    context = {
        "books": books   # Assign the object set from Class instances to a dictionary key "books"
    }
    
    #print(context)  # {'books': <QuerySet [<Book: Book object (1)>]>}
                    # {'books': <QuerySet [<Book: The Fall of Paris by Ilya Ehrenburg>]>}
    
    return render(request, 'book/book_list.html', context)

    # articles =  Article.objects.all() --> object

def show_book(request, pk):
    print(type(pk))
    context = {
        "book": Book.objects.get(pk=pk) if Book else None,
    }
    return render(request, "book/detail.html", context)

def create_book(request):
    if request.method == "POST" and BookForm: # If HTTP request is GET and the BookForm class exists, continue:
    # request = HttpRequest
    # HttpRequest.method will return a string representing the HTTP method used in the request. 
    # In this case it is a "POST" and this is guaranteed to be UPPERCASE. 
        form = BookForm(request.POST) # form variable will hold an instance of the BookForm class
        if form.is_valid():  # The is_valid() method is used to perform validation for each field of the form, it is defined in Django Form class. It returns True if data is valid and place all data into a cleaned_data attribute.
            book = form.save() # To save changes to an object that's already in the database, use save() .
            return redirect("book_detail", pk=book.pk) # Just combine the HTML template book_detil with the newly created book's primary key
    elif BookForm:
        form = BookForm()
    else:
        form = None
    
    context = {
        "form": form,
    }
    return render(request, "book/create.html", context)    

def delete_book(request, pk):
    # dictionary for initial data with
    # field names as keys
    context = {}

    # fetch the object related to passed id and assign to the book variable
    book = get_object_or_404(Book, pk = pk)

    if request.method == "POST":
        # delete object
        book.delete()
        # after deleting redirect to the book list page
        return HttpResponseRedirect("/books")

    return render(request, 'book/delete_book.html', context)


def update_book(request, pk):
    # dictionary for initial data with
    # field names as keys
    context ={}

    # fetch the object related to passed id
    book = get_object_or_404(Book, pk = pk)

    # pass the object as instance in form
    form = BookForm(request.POST or None, instance = book)
    print(form.is_valid())  # If the form is valid, then save it then redidect and 
    if form.is_valid():     # terminate the update function by the return
        form.save()
        return HttpResponseRedirect("/books/"+ str(pk))

    # If the form is not valid, skip line 83 then create a form with the incorrect information,
    # remain on the update page, continue to display/remain on the update page 
    context["form"] = form
    return render(request, "book/update_book.html", context)



def show_magazines(request):
    # In the render function, the third parameter will be the data from the database to be sent to the template "books/book_list.html"
    # {'books':books}

    magazines = Magazine.objects.all()
    # context = {
    #     "books": books if Book else [{"name": "Repository Empty"}]
    # }

    context = {
        "magazines": magazines,   # Assign the object set from Class instances to a dictionary key "books"
    }
    
    #print(context) # {'books': <QuerySet [<Book: Book object (1)>]>}
                    # {'books': <QuerySet [<Book: The Fall of Paris by Ilya Ehrenburg>]>}
    
    return render(request, 'magazine/magazine_list.html', context)

    # articles =  Article.objects.all() --> object


def create_magazine(request):
    if request.method == "POST" and MagazineForm: # If HTTP request is GET and the BookForm class exists, continue:
    # request = HttpRequest
    # HttpRequest.method will return a string representing the HTTP method used in the request. 
    # In this case it is a "POST" and this is guaranteed to be UPPERCASE. 
        form = MagazineForm(request.POST) # form variable will hold an instance of the BookForm class
        if form.is_valid():  # The is_valid() method is used to perform validation for each field of the form, it is defined in Django Form class. It returns True if data is valid and place all data into a cleaned_data attribute.
            magazine = form.save() # To save changes to an object that's already in the database, use save() .
            return redirect("magazine_detail", pk=magazine.pk) # Just combine the HTML template book_detil with the newly created book's primary key
    elif BookForm:
        form = MagazineForm()
    else:
        form = None
    
    context = {
        "form": form,
    }
    return render(request, "magazine/create.html", context)  


def show_magazine(request, pk):
    context = {
        "magazine": Magazine.objects.get(pk=pk) if Magazine else None,
    }
    return render(request, "magazine/detail.html", context)

def delete_magazine(request, pk):
    # dictionary for initial data with
    # field names as keys
    context = {}

    # fetch the object related to passed id and assign to the book variable
    magazine = get_object_or_404(Magazine, pk = pk)

    if request.method == "POST":
        # delete object
        magazine.delete()
        # after deleting redirect to the book list page
        return HttpResponseRedirect("/magazines")

    context = {
        "magazine": magazine,
    }
    return render(request, 'magazine/delete_magazine.html', context)



def update_magazine(request, pk):
    # dictionary for initial data with
    # field names as keys
    context ={}

    # fetch the object related to passed id
    magazine = get_object_or_404(Magazine, pk = pk)

    # pass the object as instance in form
    form = MagazineForm(request.POST or None, instance = magazine)
    
    # If the form is valid, then save it then redidect and 
    if form.is_valid():     # terminate the update function by the return
        form.save()
        return HttpResponseRedirect("/magazines/"+ str(pk))

    # If the form is not valid, skip line 83 then create a form with the incorrect information,
    # remain on the update page, continue to display/remain on the update page 
    context["form"] = form
    return render(request, "magazine/update_magazine.html", context)



def show_genre_str(request, genre_name):
    genre = Genre.objects.get(name=genre_name.capitalize())

    magazines = genre.magazines.all()
    
    context = {
        "magazines": magazines
    }
    return render(request, 'magazine/show_genre.html', context)


def show_genre(request, pk):
    genre = Genre.objects.get(pk=pk)
    magazines = genre.magazines.all()
    
    context = {
        "magazines": magazines
    }
    return render(request, 'magazine/show_genre.html', context)