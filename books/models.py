from django.db import models
from django.contrib.auth.models import User


class Book(models.Model):

    title = models.CharField(max_length=200, unique=True)
    author = models.ManyToManyField("Author", related_name="books")
    pages = models.SmallIntegerField(null=True)
    isbn = models.BigIntegerField(null=True)
    year_published = models.SmallIntegerField(null=True)
    description = models.TextField(null=True)
    image = models.URLField(null=True, blank=True)
    inprint = models.BooleanField(null=True)
    
    
    def __str__(self):
        return self.title + " by " + str(self.authors.first())


class  Author(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name


class  BookReview(models.Model):
    reviewer = models.ForeignKey(User, related_name="reviews_by_this_user", on_delete=models.CASCADE)
    book = models.ForeignKey(Book, related_name="reviews", on_delete=models.CASCADE)
    text = models.TextField()


class Magazine(models.Model):
    class CyclePeriod(models.TextChoices):
        WEEKLY = 'Weekly', ('Weekly')
        MONTHKY = 'Monthly', ('Monthly')
        QUARTELY = 'Quartely', ('Quartely')
        YEARLY = 'Yearly', ('Yearly')

    title = models.CharField(max_length=200, unique=True)
    cycle = models.CharField(
        max_length=10,
        choices=CyclePeriod.choices,
        default=CyclePeriod.QUARTELY,
    )
    description = models.TextField(null=True)
    image = models.URLField(null=True, blank=True)
    genre = models.ForeignKey("Genre", on_delete = models.CASCADE, related_name="magazines")  


    def __str__(self):
        return self.title 


class Genre(models.Model):
    name = models.CharField(max_length = 50)
    def __str__(self):
        return self.name


class Issue(models.Model):
    magazine = models.ForeignKey(Magazine, related_name="issues", on_delete=models.CASCADE)
    image = models.URLField(null=True, blank=True)
    title = models.CharField(max_length=200, unique=True)
    issue = date = models.SmallIntegerField(null=True)
    date = models.SmallIntegerField(null=True)
    pages = models.SmallIntegerField(null=True)
    description = models.TextField(null=True)
    def __str__(self):
        return self.title